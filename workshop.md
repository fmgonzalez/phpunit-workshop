# Workshop

## Instalar PHPUnit

1. Descargar el phar


    $ wget -O phpunit https://phar.phpunit.de/phpunit-7.phar    
    $ chmod +x phpunit 
    $ ./phpunit --version 
    
    
2. Vía composer (local al proyecto)


    $ composer require --dev phpunit/phpunit ^7
    $ ./vendor/bin/phpunit --version 
    

3. Vía composer (global)


    $ composer global require phpunit/phpunit ^7
    $ ~/.composer/vendor/bin/phpunit --version
    
    // Para *actualizar* el global: 
    $ composer global update phpunit/phpunit 
    
    
**NOTA:** Añade al PATH en ~/.bash_profile (o ~/.bashrc)

    export PATH=./vendor/bin:~/.composer/vendor/bin:$PATH
    
## Ejecutar el primer test

    $ ./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/SimpleTest.php

## Omitir tests 

- _**$this->markTestIncomplete()**_
    
    Sirve para marcar un test como "aún no implementado" o que aún no cubre el caso de uso como para confiar en él.

- _**$this->markTestSkipped()**_ 
    
    Sirve para saltar un test que no es adecuado en un determinado entorno. Por ejemplo, si un determinado driver o extensión no está disponible. 

## Aserciones

- _**assertTrue($actual)**_: El argumento debe ser true
- _**assertFalse($actual)**_: El argumento debe ser false
- _**assertSame($expected, $actual)**_: Los argumentos deben tener el mismo valor y ser del mismo tipo. Si son objetos, deben apuntar a la misma instancia del objeto.
- _**assertEquals($expected, $actual)**_: Los argumentos deben tener el mismo valor, pero no tienen que ser del mismo tipo. Si son objetos, sus atributos deben tener los mismos valores. 

## Nomenclatura 

- testSimple
- test_intl_extension_is_loaded
- it_should_be_an_incompleted_test

## Ejecutar multiples tests

    $ ./vendor/bin/phpunit --bootstrap vendor/autoload.php tests

## Switches

- _**--group [nombre-de-grupo]**__ Ejecuta los tests de un grupo
- _**--filter [nombre-de-clase]**_ o _**--filter [nombre-de-metodo]**_ Filtra la ejecución de clases o determidados tests que coincidan con el filtro.

## Añadir un phpunit.xml 

- Podemos omitir parámetros en la línea de comandos 
- Permite configurar más opciones
- Automatización 

## setUp y tearDown

- _**setUp()**_ Se ejecuta siempre _antes de cada_ test
- _**tearDown()**_ Se ejecuta siempre _después de cada_ test
- _**setUpBeforeClass()**_ Se ejecuta una única vez _antes de todos_ test
- _**tearDownAfterClass()**_ Se ejecuta una única vez_después de todos_ test

## Coverage report 

    $ phpunit -c phpunit.coverage.xml 
    
    