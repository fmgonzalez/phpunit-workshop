<?php

use PHPUnit\Framework\TestCase;

class SimpleTest extends TestCase
{
    public function testSimple()
    {
        $this->assertTrue(true);
    }

    public function test_intl_extension_is_loaded()
    {
        if (extension_loaded('intl')) {
            $this->markTestSkipped('Extension not loaded');
        }

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function it_should_be_an_incompleted_test()
    {
        $this->markTestIncomplete('Incomplete test');

        $this->assertFalse(false);
    }
}
