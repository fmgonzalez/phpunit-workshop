<?php

use PHPUnit\Framework\TestCase;

/**
 * @group User
 */
class IsValidUserNameTest extends TestCase
{
    public function testInvalidUserName()
    {
        $invalidUserNames = ['a', 'ab', 'abc', 'abc1', 'reallylongadninvalidusername'];

        foreach ($invalidUserNames as $invalidUserName) {
            $this->assertFalse(isValidUserName($invalidUserName));
        }
    }

    public function testValidUserName()
    {
        $validUserNames = ['fmgonzalez', 'joseLuis', 'carlos', 'victor', 'jose2018'];

        foreach ($validUserNames as $validUserName) {
            $this->assertTrue(isValidUserName($validUserName));
        }
    }
}
