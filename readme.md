# PHPUnit workshop

## Los tests proporcionan

- Robustez 
- Confianza
- Seguridad

_Los tests garantizan que todo funciona como debería._ **(FALSO)**

Los tests garantizan que el código hace lo que está probado.

No te garantizan que todo funcionará al 100%, pero siempre será mejor que sin tests.
 
Pocos tests son mejor que ningún test. Automatización.

## Tipos de tests

| Simple | Unitarios | Unitarios | Función |
|:---:|:---:|:---:|:---:|
| Complejo | Integración | Servicios | Comunicación entre componentes |
| Más complejo | Aceptación | UI | Click en una rejilla para ordenar un listado |

Los tests deben ser sencillos. Sin añadir demasiada lógica. 

Paradoja: si son demasiado complicados, quizás necesiten ser testeados.

## Otros conceptos

- Suites de tests
- smoke tests
- tests de regresión 
- Gherkin
- TDD (Test Driven Development)
- Mocks (inyección de dependencias), stubs, fixtures,...

## ¿Qué probar?

- ¿Setters y getters? NO 
- ¿funcionalidades del framework? NO 
- Tu propio código. 
- Funciones, classes, comunicación entre componentes,... 

## ¿Cómo probar?

Depende del tipo de tests. Pueden ser más complejos o menos, pero en general hay que intentar seguir la siguiente estructura: 

- **Arrange**: Definir un estado del SUT (sujeto del test), definir un escenario
- **Act**: Invocar un método del SUT y, probablemente, guardar un resultado. 
- **Assert**: Realizar una aserción sobre el resultado o el estado del SUT.

Pero crear buenos tests requiere de experiencia, y lo mejor para conseguirla es experimentar.

## Herramientas

- PHPUnit
- Mockery 
- Faker
- BDUnit
- PHPSpec
- Behat
- Codeception
