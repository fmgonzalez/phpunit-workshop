<?php

function isValidUserName($userName)
{
    $isValid = false;

    $minUserNameLength = 6;

    if (strlen($userName) >= $minUserNameLength) {
        $maxUserNameLength = 25;

        if (strlen($userName) < $maxUserNameLength) {
            $isAlphanumeric = preg_match("/[^a-zA-Z0-9]+/", $userName);

            $isValid = $isAlphanumeric == 0;
        }
    }

    return $isValid;
}